Solution

- Domain
	System.Web.Mvc, System.ComponentModel.DataAnnotations
- WebUI
	Ninject, Moq
- UnitTests
	Ninject, Moq, System.Web.Mvc, System.Web, Microsoft.CSharp

-------------------------------------------------------------------

Setting Up the DI Container

\Infrastructure\NinjectControllerFactory