﻿
---- Infrastructure -----------------------------------------------------------------------
1) NinjectControllerFactory
public class NinjectControllerFactory : DefaultControllerFactory

---- Global.asax.cs -----------------------------------------------------------------------

1) Registering the NinjectControllerFactory with the MVC Framework
ControllerBuilder.Current.SetControllerFactory(newNinjectControllerFactory());

--- App_Start/RouteConfig.cs --------------------------------------------------------------
1) Setting the Default Route: to tell the MVC Framework that requests that arrive for the 
root of our site (http://mysite/) should be mapped to the List action method in the 
ProductController class.

routes.MapRoute(name: "Default",
	url: "{controller}/{action}/{id}",
	defaults: new { controller = "Product", action = "List", *****
	id = UrlParameter.Optional }
);

2) Custom Model Binder

We need to tell the MVC Framework that it can use our CartModelBinder class to create instances 
of Cart. We do this in the Application_Start method of Global.asax

protected void Application_Start() {
...
	ModelBinders.Binders.Add(typeof(Cart), new CartModelBinder());
}

---- Web.config (Root folder) ------------------------------------------------------------
1) Entity Framework Context
To tell the Entity Framework how to connect to the database, we do that by adding a
database connection string to the Web.config file in the SportsStore.WebUI project with the 
same name as the context class

<connectionStrings>
	<!-- Created by VS <add name="DefaultConnection" providerName="System.Data.SqlClient" connectionString="Data Source=(LocalDb)\v11.0;Initial Catalog=aspnet-WebUI-20150612215734;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\aspnet-WebUI-20150612215734.mdf" />-->
	<add name="EFDbContext" connectionString="Data Source=(localdb)\v11.0;Initial Catalog=SportsStore;Integrated Security=True" providerName="System.Data.SqlClient" />
  </connectionStrings>

  **** put everything on a single line

2) Settings

We read the value for a property using the ConfigurationManager.AppSettings property, 
which allows us to access application settings we have placed in the Web.config file (in the root folder)
<appSettings>
...
	<add key="Email.WriteAsFile" value="true"/>
</appSettings> 

3) Authentication

<authentication mode="Forms">
	<forms loginUrl="~/Account/Login" timeout="2880">
		<credentials passwordFormat="Clear">
		<user name="admin" password="secret" />
		</credentials>
	</forms>
</authentication>

---- HtmlHelpers --------------------------------------------------------------------------
1) PagingHelpers: The PageLinks extension method generates the HTML for a set of page links 
using the information provided in a PagingInfo object. The Func parameter provides the 
ability to pass in a delegate that will be used to generate the links to view other pages.

<div class="pager">
	@Html.PageLinks(Model.PagingInfo, x => Url.Action("List", new {page = x, category = Model.CurrentCategory}))
</div>

---- Views/Web.config File ----------------------------------------------------------------
1) <namespaces>

An extension method is available for use only when the namespace that contains it is in scope. 
In a code file, this is done with a using statement; but for a Razor view, we must add a 
configuration entry to the Web.config file Every namespace that we need to refer to in a Razor 
view needs to be declared either in this way or in the view itself with a @using statement.

<namespaces>
	<add namespace="System.Web.Mvc" />
	<add namespace="System.Web.Mvc.Ajax" />
	<add namespace="System.Web.Mvc.Html" />
	<add namespace="System.Web.Optimization"/>
	<add namespace="System.Web.Routing" />
	<add namespace="SportsStore.WebUI.HtmlHelpers"/>
</namespaces>

---- RouteConfig.cs ----------------------------------------------------------------------
1) Improving the URLs

Add a new route to the RegisterRoutes method in the RouteConfig.cs in the App_Start folder
http://localhost/?page=2 --> http://localhost/Page2

routes.MapRoute(name: null,
	url: "Page{page}",
	defaults: new { Controller = "Product", action = "List" }
);

---- _Layout.cshtml ---------------------------------------------------------------------
1)  Defining Common Content in the Layout 

When we created the List.cshtml view for the Product controller, we asked you to check the 
option to use a layout, but leave the box that specifies a layout blank. This has the effect 
of using the default layout, _Layout.cshtml, which can be found in the Views/Shared

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width" />
	<title>@ViewBag.Title</title>
	<!-- Adding CSS Styles -->
	<link href="~/Content/Site.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div id="header">
		<div class="title">SPORTS STORE</div>
	</div>
	<div id="categories">
		We will put something useful here later
	</div>
	<div id="content">
		@RenderBody()
	</div>
</body>
</html>

---- Using Model Binding ---------------------------------------------------------------
The MVC Framework uses a system called model binding to create C# objects from HTTP requests
in order to pass them as parameter values to action methods. This is how MVC processes forms, 
for example. Model binders can create C# types from any information that is available in the 
request. This is one of the central features of the MVC Framework. We are going to create a 
custom model binder to improve our CartController class.

1) Creating a Custom Model Binder
We create a custom model binder by implementing the IModelBinder interface. Create a new folder
in the SportsStore.WebUI project called Binders and create a CartModelBinder class

Adding a Cart parameter to each of the action methods, when the MVC Framework receives a request 
that requires say, the AddToCart method to be invoked, it begins by looking at the parameters 
for the action method. It looks at the list of binders available and tries to find one that can 
create instances of each parameter type.

---- Validations -------------------------------------------------------------------------
1) Javascripts for validations

<script src="~/Scripts/jquery-1.7.1.js"></script>
<script src="~/Scripts/jquery.validate.min.js"></script>
<script src="~/Scripts/jquery.validate.unobtrusive.min.js"></script>

With these additions, client-side validation will work for our administration views. The appearance of
error messages to the user is the same because the CSS classes that are used by the server validation are
also used by the client-side validation—but the response is immediate and does not require a request to
be sent to the server. In most situations, client-side validation is a useful feature, but if, for some reason,
you do not want to validate at the client, you need to use the following statements:
...
HtmlHelper.ClientValidationEnabled = false;
HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
...
If you put these statements in a view or in a controller, then client-side validation is disabled only for
the current action. You can disable client-side validation for the entire application by using those
statements in the Application_Start method of Global.asax or by adding values to the Web.config file,
like this:
...
<configuration>
	<appSettings>
	<add key="ClientValidationEnabled" value="false"/>
	<add key="UnobtrusiveJavaScriptEnabled" value="false"/>
	</appSettings>
</configuration>
...