﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SportsStore.WebUI
{
    public class RouteConfig
    {
        /// <summary>
        /// To be very clear: the routing system doesn’t try to find the route that provides the best matching route. It
        /// finds only the first match, at which point it uses the route to generate the URL; any subsequent routes are
        /// ignored. For this reason, you should define your most specific routes first
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            // important that you add this route before the Default one that is already in the file. 
            // routes are processed in the order they are listed, and we need our new route to take
            // precedence over the existing one
            routes.MapRoute("AllProductsRoute", "", new
            {
                controller = "Product",
                action = "List",
                category = (string)null,
                page = 1
            }
            );

            routes.MapRoute("PageProductsRoute", "Page{page}", new
            {
                controller = "Product",
                action = "List",
                category = (string)null
            },
                new { page = @"\d+" }
             );

            routes.MapRoute("CategoryProductsRoute", "{category}", new
            {
                controller = "Product",
                action = "List",
                page = 1
            }
            );

            routes.MapRoute("CategoryPageProductsRoutes", "{category}/Page{page}", new
            {
                controller = "Product",
                action = "List"
            },
                new { page = @"\d+" }
            );

            routes.MapRoute("StandardRoute", "{controller}/{action}");

            // To disable searching for controllers in other namespaces, we take the Route object 
            // and set the UseNamespaceFallback key in the DataTokens collection property to false.
            foreach (RouteBase routeBase in routes)
            {
                if (routeBase is Route)
                {
                    Route route = (Route)routeBase;
                    //route.DataTokens = route.DataTokens ?? new RouteValueDictionary();
                    //route.DataTokens["UseNamespaceFallback"] = false;
                    //route.DataTokens["Namespaces"] = new string[] { "SportsStore.WebUI.Controllers" };
                    //route.Constraints.Add("httpMethod", new HttpMethodConstraint("GET", "POST"));
                    //Custom Constraint
                    //route.Constraints.Add("userAgent", new SportsStore.WebUI.Infrastructure.Concrete.UserAgentConstraint("Chrome"));

                }
            }
        }
    }
}