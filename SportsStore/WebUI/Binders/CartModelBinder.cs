﻿using SportsStore.Domain.Entities;
using System;
using System.Web.Mvc;

namespace SportsStore.WebUI.Binders
{
    /// <summary>
    /// The IModelBinder interface defines one method: BindModel. The two parameters are provided to make
    /// creating the domain model object possible. The ControllerContext provides access to all the information
    /// that the controller class has, which includes details of the request from the client. The
    /// ModelBindingContext gives you information about the model object you are being asked to build and some
    /// tools for making the binding process easier.
    /// </summary>
    public class CartModelBinder : IModelBinder
    {
        private const String sessionKey = "Cart";

        public Object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            // get the Cart from the session
            Cart cart = (Cart)controllerContext.HttpContext.Session[sessionKey];
            // create the Cart if there wasn't one in the session data
            if (cart == null)
            {
                cart = new Cart();
                controllerContext.HttpContext.Session[sessionKey] = cart;
            }
            // return the cart
            return cart;
        }
    }
}