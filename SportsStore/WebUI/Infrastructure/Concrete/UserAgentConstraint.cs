﻿using System;
using System.Web;
using System.Web.Routing;

namespace SportsStore.WebUI.Infrastructure.Concrete
{
    public class UserAgentConstraint : IRouteConstraint
    {
        private String requiredUserAgent;

        public UserAgentConstraint(String agentParam)
        {
            requiredUserAgent = agentParam;
        }

        public Boolean Match(HttpContextBase httpContext, Route route, String parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return httpContext.Request.UserAgent != null && httpContext.Request.UserAgent.Contains(requiredUserAgent);
        }
    }
}