﻿using Moq;
using Ninject;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Concrete;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Infrastructure.Abstract;
using SportsStore.WebUI.Infrastructure.Concrete;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace SportsStore.WebUI.Infrastructure
{
    /// <summary>
    /// Custom dependency
    /// Use the NinjectController class to create controller objects.
    /// </summary>
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            return controllerType != null ? (IController)ninjectKernel.Get(controllerType) : null;
        }

        private void AddBindings()
        {
            /*
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(m => m.Products).Returns(new List<Product> {
                new Product { Name = "Football", Price = 25, Description = "Net" },
                new Product { Name = "Surf board", Price = 179 },
                new Product { Name = "Running shoes", Price = 95 }
            }.AsQueryable()); // The AsQueryable method is a LINQ extension method 
            // that transforms an IEnumerable<T> into an IQueryable<T>,
            // which we need to match our interface signature

            // Ninject to return the same mock object whenever it gets a request for an implementation of
            // the IProductRepository interface, which is why we have used the ToConstant method,
            ninjectKernel.Bind<IProductRepository>().ToConstant(mock.Object);*/

            ninjectKernel.Bind<IProductRepository>().To<EFProductRepository>();

            EmailOrderProcessor.EmailSettings emailSettings = new EmailOrderProcessor.EmailSettings
            {
                WriteAsFile = bool.Parse(ConfigurationManager.AppSettings["Email.WriteAsFile"] ?? "false")
            };

            ninjectKernel.Bind<IOrderProcessor>().To<EmailOrderProcessor>().WithConstructorArgument("settings", emailSettings);
            ninjectKernel.Bind<IAuthProvider>().To<FormsAuthProvider>();
        }

    }
}