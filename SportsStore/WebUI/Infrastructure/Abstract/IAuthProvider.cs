﻿using System;

namespace SportsStore.WebUI.Infrastructure.Abstract
{
    public interface IAuthProvider
    {
        Boolean Authenticate(String username, String password);
    }
}
