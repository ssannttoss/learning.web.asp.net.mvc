Dependency injection cotainer, also known as an IoC container

- Ninject www.ninject.org
- Microsoft created its own DI container, called Unity unity.codeplex.com.

===========================================================================

Automated Testing

- unit testing, which is a way to specify and verify the behavior of individual classes (or other small units of code) in isolation from the rest of the application

pattern known as arrange/act/assert (A/A/A). Arrange refers
to setting up the conditions for coffee the test, act refers to performing the test, and assert refers to verifying that
the result was  one that was required

- integration testing, which is a way
to specify and verify the behavior of multiple components working together, up to and including the entire Web application.

For Web applications, the most common approach to integration testing is UI automation, which means simulating or automating a Web browser to exercise the application�s entire technology stack by reproducing the actions that a user would perform, such as pressing buttons, following links, and
submitting forms. The two best-known open source browser automation options for .NET developers are

� Selenium RC (http://seleniumhq.org/), which consists of a Java �server�
application that can send automation commands to Internet Explorer, Firefox,
Safari, or Opera, plus clients for .NET, Python, Ruby, and multiple others so that you
can write test scripts in the language of your choice. Selenium is powerful and
mature; its only drawback is that you have to run its Java server.
� WatiN (http://watin.org), a .NET library that can send automation commands to
Internet Explorer or Firefox. Its API isn�t as powerful as Selenium, but it comfortably
handles most common scenarios and is easy to set up�you need only reference a
single DLL.

==============================================================================

Razor

It is important to understand the difference between omitting the Layout property from the view file and setting it to null. If your view is self-contained and you do not want to use a layout, then set the Layout property to null. If you omit the Layout property, then the MVC framework will assume that you do want a layout and that it should use the value it finds in the view start file


==============================================================================

Essential Tools

three tools that should be part of every MVC programmer�s arsenal:
a dependency injection (DI) container [Ninject, Unit], a unit test framework [VS built in, NUnit], and a mocking tool [Moq, Rhino Mocks].


Ninject supports a number of different conditional binding methods, the most useful of which we have listed in Table 6-1.
Table 6-1. Ninject Conditional Binding Methods

When(predicate) Binding is used when the predicate�a lambda expression�evaluates totrue.
WhenClassHas<T>() Binding is used when the class being injected is annotated with the attribute whose type is specified by T.
WhenInjectedInto<T>() Binding is used when the class being injected into is of type T.

Moq

Moq evaluates the behaviors it has been given in reverse order, so that the most recent calls to the Setup method are considered first.