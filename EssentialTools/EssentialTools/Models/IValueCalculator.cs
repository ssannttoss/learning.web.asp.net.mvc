﻿using System;
namespace EssentialTools.Models
{
    public interface IValueCalculator
    {
        decimal ValueProducts(System.Collections.Generic.IEnumerable<Product> products);
    }
}
